from django.shortcuts import render

def myself(request):
    return render(request, "home.html")

def error404(request, exception):
    return render(request, "404.html")